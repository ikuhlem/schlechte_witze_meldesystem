from flask import (
    Blueprint, flash, redirect, render_template, session, url_for, request
)

from .add_comedian_form import AddComedianForm
from ..persistence.db import get_db
from ..model.joke import Joke

bp = Blueprint('admin', __name__, url_prefix='/admin')

@bp.route("/", methods=['GET', 'POST'])
def show_admin_page():
    if not session.get('logged_in'):
        flash("Nicht eingeloggt!")
        return redirect(url_for("login.show_login_form"), code=302)
    if not session.get('is_admin'):
        flash("Nicht als Admin eingeloggt!")
        return redirect(url_for("jokes.show_joke_list"), code=302)
    return render_template('admin.html', scripts=['admin.js'],
                           stylesheets=['admin.css'],
                           add_form=AddComedianForm())

@bp.route("/add-comedian", methods=["POST"])
def add_comedian():
    name = request.form['name']
    name = name.lower()
    if not session.get('logged_in'):
        flash("Nicht eingeloggt!")
        return redirect(url_for("login.show_login_form"), code=302)
    if not session.get('is_admin'):
        flash("Nicht als Admin eingeloggt!")
        return redirect(url_for("jokes.show_joke_list"), code=302)
    
    db_add_comedian(name)
    return "{} Hinzugefügt!?".format(name)


def db_add_comedian(name):
    insert_string = "INSERT INTO comedians VALUES (?, 0)"
    insert_tuple = (name,)
    connection = get_db()
    cursor = connection.cursor()
    cursor.execute(insert_string, insert_tuple)
    connection.commit()

@bp.route('/toggle-payed-for', methods=["POST"])
def toggle_payed_for():
    id_ = request.form['id']
    r = db_toggle_payed_for(id_)
    if r:
        return "true"
    return "false"

def db_toggle_payed_for(joke_id):

    connection = get_db()
    cursor = connection.cursor()
    
    joke = db_get_joke_by_id(joke_id, cursor)
    if joke.payed_for:
        new_state = False
    else:
        new_state = True
    update_string = "UPDATE jokes SET payed_for = ? WHERE _id = ?"
    cursor.execute(update_string, (new_state, joke_id))
    connection.commit()
    return new_state

def db_get_joke_by_id(joke_id, cursor):
    select_string = "SELECT * FROM jokes WHERE _id = ?"
    cursor.execute(select_string, (joke_id,))
    joke_tuple = cursor.fetchone()
    return Joke.map_from_tuple(joke_tuple)
    
