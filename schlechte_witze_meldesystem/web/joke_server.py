from flask import (render_template, redirect, flash,
                   url_for, session, jsonify)
from werkzeug.security import check_password_hash
from datetime import datetime
from .login_form import LoginForm
from .add_comedian_form import AddComedianForm
from ..model.joke import Joke


class JokeServer:

    def __init__(self):
        self.db_interface = None
        self.config_io = None

    def handle_get_jokes(self, db_interface):
        jokes = db_interface.get_joke(select_max=100)
        joke_dicts = []
        for j in jokes:
            joke_dicts.append({'id': j.id,
                               'date': j.date,
                               'comedian': j.comedian,
                               'punchline': j.punchline,
                               'context': j.context,
                               'payed_for': j.payed_for})
        return jsonify(joke_dicts)

    def serve_joke_list(self, db_interface, n_max_jokes):
        if not session.get('logged_in'):
            flash("Nicht eingeloggt!")
            return redirect(url_for("show_login_form"), code=302)
        return render_template("joke_list.html", scripts=['joke_list.js'])

    def serve_joke_form(self, db_interface):
        if not session.get('logged_in'):
            flash("Nicht eingeloggt!")
            return redirect(url_for("show_login_form"), code=302)
        return render_template("joke_form.html", comedians=db_interface.get_comedians())

    def handle_joke_form(self, form_dict, db_interface):
        if not session.get('logged_in'):
            flash("Nicht eingeloggt!")
            return redirect(url_for("show_login_form"), code=302)
        j = Joke()
        j.comedian = form_dict['comedian']
        j.context = form_dict['context']
        j.punchline = form_dict['punchline']
        if 'payed' in form_dict:
            j.payed_for = True
        now = datetime.now()
        date_str = now.strftime("%Y-%m-%d")
        j.date = date_str
        db_interface.save_joke(j)

    def serve_login_form(self):
        if session.get('logged_in'):
            return redirect(url_for('show_joke_list', code=302))        
        form = LoginForm()
        if form.validate_on_submit():
            if not check_password_hash(self.config_io.get_hashed_password(),
                                       form.password.data)\
                and not check_password_hash(self.config_io.get_hashed_password_admin(),
                                            form.password.data):
                flash("Falsches Passwort!")
                return redirect(url_for("show_login_form"), code=302)
            if check_password_hash(self.config_io.get_hashed_password_admin(), form.password.data):
                session['is_admin'] = True
            session['logged_in'] = True
            return redirect(url_for('show_joke_list'), code=302)
        return render_template('login.html', title='Einloggen', form=form)

    def serve_admin_page(self):
        if not session.get('logged_in'):
            flash("Nicht eingeloggt!")
            return redirect(url_for("show_login_form"), code=302)
        if not session.get('is_admin'):
            flash("Nicht als Admin eingeloggt!")
            return redirect(url_for("show_joke_list"), code=302)
        return render_template('admin.html', scripts=['admin.js'],
                               stylesheets=['admin.css'],
                               add_form=AddComedianForm())

    def handle_add_comedian(self, name, db_interface):
        name = name.lower()
        if not session.get('logged_in'):
            flash("Nicht eingeloggt!")
            return redirect(url_for("show_login_form"), code=302)
        if not session.get('is_admin'):
            flash("Nicht als Admin eingeloggt!")
            return redirect(url_for("show_joke_list"), code=302)
        db_interface.add_comedian(name)
        return "{} Hinzugefügt!?".format(name)
    
