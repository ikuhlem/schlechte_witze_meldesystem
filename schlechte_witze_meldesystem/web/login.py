from flask import (
    Blueprint, flash, redirect, render_template, session, url_for, current_app
)
from werkzeug.security import check_password_hash

from .login_form import LoginForm

bp = Blueprint('login', __name__, url_prefix='/')

@bp.route("/", methods=['GET', 'POST'])
def show_login_form():
    if session.get('logged_in'):
        return redirect(url_for('jokes.show_joke_list', code=302))        
    form = LoginForm()
    if form.validate_on_submit():
        if not check_password_hash(current_app.config['USER_PASSWORD'],
                                   form.password.data)\
            and not check_password_hash(current_app.config['ADMIN_PASSWORD'],
                                        form.password.data):
            flash("Falsches Passwort!")
            return redirect(url_for("login.show_login_form"), code=302)
        if check_password_hash(current_app.config['ADMIN_PASSWORD'], form.password.data):
            session['is_admin'] = True
        session['logged_in'] = True
        return redirect(url_for('jokes.show_joke_list'), code=302)
    return render_template('login.html', title='Einloggen', form=form)

@bp.route('/logout')
def logout_please():
    session.pop('logged_in', None)
    session.pop('is_admin', None)
    flash("Du wurdest ausgeloggt")
    return redirect(url_for('login.show_login_form'), code=307)
