from datetime import datetime
from flask import (
    Blueprint, flash, redirect, render_template, session, url_for, request, jsonify
)
from ..persistence.db import get_db
from ..model.joke import Joke

bp = Blueprint('jokes', __name__, url_prefix='/jokes')

@bp.route("/list", methods=['GET', 'POST'])
def show_joke_list():
    if not session.get('logged_in'):
        flash("Nicht eingeloggt!")
        return redirect(url_for("login.show_login_form"), code=302)
    return render_template("joke_list.html", scripts=['joke_list.js'])

@bp.route("/form", methods=['GET', 'POST'])
def show_joke_form():
    if not session.get('logged_in'):
        flash("Nicht eingeloggt!")
        return redirect(url_for("login.show_login_form"), code=302)
    return render_template("joke_form.html", comedians=get_comedians())

@bp.route('/handle-form', methods=['GET', 'POST'])
def handle_joke_form():
    if not session.get('logged_in'):
        flash("Nicht eingeloggt!")
        return redirect(url_for("login.show_login_form"), code=302)
    form_dict = request.form
    j = Joke()
    j.comedian = form_dict['comedian']
    j.context = form_dict['context']
    j.punchline = form_dict['punchline']
    if 'payed' in form_dict:
        j.payed_for = True
    now = datetime.now()
    date_str = now.strftime("%Y-%m-%d")
    j.date = date_str
    save_joke(j)
    return redirect(url_for('jokes.show_joke_list'), code=302)

def get_comedians():
    fetch_string = "SELECT * FROM comedians ORDER BY name ASC"
    connection = get_db()
    cursor = connection.cursor()
    cursor.arraysize = 90
    cursor.execute(fetch_string)
    comedians_fetched = cursor.fetchmany()        
    return comedians_fetched

def save_joke(joke):
    insert_string = '''INSERT INTO jokes (comedian, punchline, context, date, participants, payed_for)
    VALUES (?,?,?,?,?,?)
    '''
    insert_tuple = (joke.comedian,
                    joke.punchline,
                    joke.context,
                    joke.date,
                    joke.participants,
                    joke.payed_for)
    connection = get_db()
    cursor = connection.cursor()
    cursor.execute(insert_string, insert_tuple)
    update_string = "UPDATE comedians SET n_jokes = n_jokes + 1 WHERE name=?"
    cursor.execute(update_string, (joke.comedian,))        
    connection.commit()

@bp.route('/get-jokes', methods=["GET"])
def get_jokes():
    jokes = db_get_jokes(select_max=100)
    joke_dicts = []
    for j in jokes:
        joke_dicts.append({'id': j.id,
                           'date': j.date,
                           'comedian': j.comedian,
                           'punchline': j.punchline,
                           'context': j.context,
                           'payed_for': j.payed_for})
    return jsonify(joke_dicts)

def db_get_jokes(select_max=10):
    select_string = "SELECT * FROM jokes ORDER BY _id DESC"
    connection = get_db()
    cursor = connection.cursor()
    cursor.execute(select_string)
    tuples_fetched = cursor.fetchmany(select_max)
    jokes_fetched = []
    for i in range(len(tuples_fetched)):                       
        jokes_fetched.append(Joke.map_from_tuple(tuples_fetched[i]))        
    return jokes_fetched
