from flask import Flask
import os
here = os.path.dirname(os.path.realpath(__file__))
home = os.path.expanduser("~")


def create_app(test_config=None, **kwargs):

    app = Flask(__name__, instance_relative_config=True, template_folder=here + "/web/templates",
                static_folder=here + "/static")

    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'witze.sqlite'),
        USER_PASSWORD='pbkdf2:sha256:150000$GNJJ5jcK$730acbb49a1d837e39033bffcea2306a3961b6de91810deaa6de6c1d69f357fd',
        ADMIN_PASSWORD='pbkdf2:sha256:150000$O6j35W5A$610db1063d29c5ca1aad6ff82fa2611cd320002bddec71ac7707d66b9d4db3cb'
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    from .persistence import db
    db.init_app(app)

    from .web import login, admin, jokes
    app.register_blueprint(login.bp)
    app.register_blueprint(admin.bp)
    app.register_blueprint(jokes.bp)

    return app


app = create_app()

if __name__ == "__main__":
    app.run()
