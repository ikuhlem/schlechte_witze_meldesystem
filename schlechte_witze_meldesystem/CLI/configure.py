import sqlite3
import sys
import os
import json
from werkzeug.security import generate_password_hash

home = os.path.expanduser("~")
config_folder = home + "/.config/schlechte_witze_meldesystem"
path_config_file = config_folder + "/config"


def _create_config_folder():
    if os.path.exists(config_folder):
        return
    os.makedirs(config_folder)

    
def _read_config():
    if os.path.exists(path_config_file):
        config_file = open(path_config_file, "r")
        config_dict = json.load(config_file)
        config_file.close()
        return config_dict
    return _create_default_config_dict()


def _write_config(config_dict):
    config_file = open(path_config_file, "w")    
    json.dump(config_dict, config_file)
    config_file.close()

    
def _create_default_config_dict():
    d = {}
    path_to_db = config_folder + "/schlechte_witze.db"
    d["path_to_data_base"] = path_to_db
    path_to_user_db = config_folder + "/user.db"
    d["path_to_user_database"] = path_to_user_db
    return d


def _initialize_db(path_to_db):
    if os.path.exists(path_to_db):
        print("file '" + path_to_db + "' already exists")
        read = input("do you want to overwrite it? [y/N] ")
        if not read == "y":
            return
        os.remove(path_to_db)
    connection = sqlite3.connect(path_to_db)
    cursor = connection.cursor()
    _create_comedians_table(cursor)
    connection.commit()
    _create_jokes_table(cursor)
    connection.commit()
    _create_withdrawals_table(cursor)
    connection.commit()

    
def _create_comedians_table(db_cursor):
    db_cursor.execute('''CREATE TABLE comedians    
    (name text PRIMARY KEY NOT NULL, n_jokes int)''')

    
def _create_jokes_table(db_cursor):
    create_string = '''CREATE TABLE jokes (
    _id integer PRIMARY KEY AUTOINCREMENT,
    comedian text NOT NULL,
    punchline text NOT NULL,
    context text,
    date text NOT NULL,
    participants text,
    payed_for integer,
    FOREIGN KEY (comedian) REFERENCES comedians(name)
    )    
    '''
    db_cursor.execute(create_string)

    
def _create_withdrawals_table(db_cursor):
    db_cursor.execute('''CREATE TABLE withdrawals (
    _id integer PRIMARY KEY NOT NULL,
    date text NOT NULL,
    amount integer NOT NULL,
    reason text NOT NULL)
    ''')

    
def _initialize_web_server_db(path_to_db):
    if os.path.exists(path_to_db):
        print("file '" + path_to_db + "' already exists")
        read = input("do you want to overwrite it? [y/N] ")
        if not read == "y":
            return
        os.remove(path_to_db)
    connection = sqlite3.connect(path_to_db)
    cursor = connection.cursor()
    _create_user_table(cursor)
    connection.commit()


def _create_user_table(db_cursor):
    create_string = '''CREATE TABLE users (
    _id integer PRIMARY KEY AUTOINCREMENT,
    name text NOT NULL,
    password_hash text NOT NULL,
    activated integer NOT NULL
    )
    '''
    db_cursor.execute(create_string)


def main():
    _create_config_folder()
    config_dict = _read_config()
    path_to_db = config_dict["path_to_data_base"]    
    if sys.argv.count("--path-to-db"):
        path_to_db = sys.argv[sys.argv.index["--path-to-db"] + 1]

    admin_pw = input(" admin password: ")
    admin_h = generate_password_hash(admin_pw)
    config_dict["hashed_password_admin"] = admin_h    
        
    pw = input(" password: ")
    h = generate_password_hash(pw)
    config_dict["hashed_password"] = h    
        
    # path_to_user_db = config_dict["path_to_user_database"]
    # if sys.argv.count("--path-to-user-db"):
    # path_to_user_db = sys.argv[sys.argv.index["--path-to-user-db"] + 1]
    config_dict["path_to_data_base"] = path_to_db
    # config_dict["path_to_user_database"] = path_to_user_db
    _initialize_db(path_to_db)
    # _initialize_web_server_db(path_to_user_db)
    _write_config(config_dict)
    

if __name__ == "__main__":
    main()
