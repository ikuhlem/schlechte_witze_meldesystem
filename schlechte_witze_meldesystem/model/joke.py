

class Joke:

    def __init__(self):
        self.id = None
        self.comedian = None
        self.punchline = None
        self.context = None
        self.date = None
        self.participants = None
        self.payed_for = None
        
    def map_from_tuple(t):
        j = Joke()
        j.id, j.comedian, j.punchline, j.context = t[:4]
        j.date, j.participants, j.payed_for = t[4:]
        return j
        
    def __str__(self):
        s = "Schlechter Witz von " + self.comedian + ": \"" + self.punchline + "\""
        return s
        
