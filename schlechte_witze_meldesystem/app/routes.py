from flask import (request, redirect, url_for, session, flash,
                   jsonify)
from schlechte_witze_meldesystem.app import app, server
from schlechte_witze_meldesystem.app.db_connect import get_db


@app.route("/jokes", methods=['GET', 'POST'])
def show_joke_list():
    db_interface = get_db()
    return server.serve_joke_list(db_interface, 30)


@app.route("/joke-form", methods=['GET', 'POST'])
def show_joke_form():
    db_interface = get_db()
    return server.serve_joke_form(db_interface)


@app.route("/handle-joke-form", methods=['GET', 'POST'])
def handle_joke_form():
    db_interface = get_db()
    server.handle_joke_form(request.form, db_interface)
    return redirect(url_for('show_joke_list'), code=302)


@app.route("/", methods=['GET', 'POST'])
@app.route("/login", methods=['GET', 'POST'])
def show_login_form():
    return server.serve_login_form()


@app.route("/logout")
def logout_please():
    session.pop('logged_in', None)
    session.pop('is_admin', None)
    flash("Du wurdest ausgeloggt")
    return redirect(url_for('show_login_form'), code=307)


@app.route("/admin", methods=["GET", "POST"])
def show_admin_page():
    return server.serve_admin_page()

@app.route("/add-comedian", methods=["POST"])
def add_comedian():
    name = request.form['name']
    db_interface = get_db()
    return server.handle_add_comedian(name, db_interface)

@app.route('/get-jokes', methods=["GET"])
def get_jokes():
    db_interface = get_db()
    return server.handle_get_jokes(db_interface)

@app.route('/toggle-payed-for', methods=["POST"])
def toggle_payed_for():
    id_ = request.form['id']
    db_interface = get_db()
    r = db_interface.toggle_payed_for(id_)
    if r:
        return "true"
    return "false"