from flask import g
from schlechte_witze_meldesystem.persistence.sqlite_interface import SQLiteInterface
from schlechte_witze_meldesystem.app import app


def connect_db():
    db_interface = SQLiteInterface()
    return db_interface


def get_db():
    if not hasattr(g, 'db_interface'):
        g.db_interface = connect_db()
    return g.db_interface


@app.teardown_appcontext
def db_close(error):
    if hasattr(g, 'db_interface'):
        g.db_interface.close()
