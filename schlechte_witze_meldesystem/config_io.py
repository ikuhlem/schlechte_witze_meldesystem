import json
import os
home = os.path.expanduser("~")


class ConfigIO:

    def __init__(self, path_to_config=home + "/.config/schlechte_witze_meldesystem/config"):
        self.path_to_config = path_to_config
        self.json_content = json.load(open(self.path_to_config, "r"))

    def get_path_to_user_db(self):
        return self.json_content["path_to_user_database"]

    def get_path_to_joke_db(self):
        return self.json_content["path_to_data_base"]

    def get_hashed_password(self):
        return self.json_content["hashed_password"]

    def get_hashed_password_admin(self):
        return self.json_content["hashed_password_admin"]
