
var jokes;

function requestJokes(url_req_jokes, urlToggle) {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function()
    {
	if(this.readyState == 4 && this.status == 200) {
	    jokes = JSON.parse(this.responseText);
	    loadNextJokes(urlToggle);
	} else {
	    jokes=[];
	}
    }
    req.open('GET', url_req_jokes, true);
    req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
    req.send();
}

function loadNextJokes(urlToggle) {
  var j;
  var table = document.getElementById("jokeTable");
  for (var i=0; i<jokes.length; i++) {
      j = jokes[i];
      var tr = "<tr class=\"";
      if (i%2 == 0) {
	  tr += "even\">";
      } else {
	  tr += "odd\">";
      }
      tr += "<td>" + j["id"] + "</td>";
      tr += "<td>" + j["date"] + "</td>";
      tr += "<td>" + j["comedian"] + "</td>";
      tr += "<td style=\"text-align: center;\">";
      if (is_admin) {
	  tr += "<button type=\"button\" id=\"btn-" + j["id"] + "\" onclick=\"togglePayedFor('" + urlToggle +"', " + j["id"] + ")\">"
      }
      if (j["payed_for"] == 1) {
	  tr += "<span style=\"color: #30A030;\">&#10004</span>";
      } else {
	  tr += "<span style=\"color: #A03030;\">&#10006</span>";
      }
      if (is_admin) {
	  tr += "</button>";
      }
      tr += "</td>";
      
      tr += "<td>" + j["punchline"] + "</td>";
      tr += "<td>" + j["context"] + "</td>";
      tr += "</tr>";
      console.log(tr);
      table.innerHTML += tr;
  }
}

function togglePayedFor(urlToggle, id_) {
  var req = new XMLHttpRequest();
  var new_state;
  var btn = document.getElementById('btn-'+id_);
  req.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      new_state = this.responseText;
      if (new_state == "true") {
        btn.innerHTML = "<span style=\"color: #30A030;\">&#10004</span>";
      } else {
        btn.innerHTML = "<span style=\"color: #A03030;\">&#10006</span>";
      }
    }
  }
  req.open('POST', urlToggle, true);
  req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
  req.send("id=" + id_);
}

requestJokes(urlGetJokes, urlToggle);
