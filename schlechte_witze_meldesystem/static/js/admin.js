
function addTab(name, idx) {
    var tabs = document.getElementById("tabs");
    tabs.innerHTML += "<button class=\"tablinks\" onclick=\"showBlock(" + idx + ")\">" + name + "</button>\n";
}

function createTabs() {
    var blocks = document.getElementsByClassName("actionBlock");
    var block, heading;
    for (i=0; i < blocks.length; i++) {
        block = blocks[i];
        heading = block.getElementsByTagName("H4")[0];
        addTab(heading.textContent, i);
    }
}

function showBlock(idx) {
    var blocks = document.getElementsByClassName("actionBlock");
    var block;
    for (i=0; i<blocks.length; i++) {
        block = blocks[i];
        if (i == idx) {
            block.style.display = "block";
        } else {
            block.style.display = "none";
        }
    }
}


function showActionBlock(id_str) {

}

createTabs();

function loadDoc() {
    document.getElementById("blurb").innerHTML += "<p>jaup!</p><br>";
}

function do_ajax(url) {
    var req = new XMLHttpRequest();
    var result = document.getElementById('result');
    req.onreadystatechange = function()
    {
      if(this.readyState == 4 && this.status == 200) {
        result.innerHTML = this.responseText;
      } else {
        result.innerHTML = "Ungültige URL? Admin rufen!...";
      }
    }
    req.open('POST', url, true);
    req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
    req.send("name=" + document.getElementById('name').value);
}