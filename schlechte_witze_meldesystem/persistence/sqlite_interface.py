import sqlite3
import json
import os
from ..model.joke import Joke
from ..config_io import ConfigIO


class SQLiteInterface:

    def __init__(self):

        path_to_db = self._read_path_to_db_from_config()
        self.connection = sqlite3.connect(path_to_db)
        self.cursor = self.connection.cursor()
        self.cursor.arraysize = 30

    def _read_path_to_db_from_config(self):
        config_io = ConfigIO()
        return config_io.get_path_to_joke_db()

    def get_joke(self, comedian="", select_max=10):
        if comedian:
            select_string = "SELECT * FROM jokes WHERE comedian=? ORDER BY _id DESC"
            self.cursor.execute(select_string, (comedian,))
        else:
            select_string = "SELECT * FROM jokes ORDER BY _id DESC"
            self.cursor.execute(select_string)
        tuples_fetched = self.cursor.fetchmany(select_max)
        jokes_fetched = []
        for i in range(len(tuples_fetched)):                       
            jokes_fetched.append(Joke.map_from_tuple(tuples_fetched[i]))        
        return jokes_fetched

    def toggle_payed_for(self, joke_id):
        joke = self.get_joke_by_id(joke_id)
        if joke.payed_for:
            new_state = False
        else:
            new_state = True
        update_string = "UPDATE jokes SET payed_for = ? WHERE _id = ?"
        self.cursor.execute(update_string, (new_state, joke_id))
        self.connection.commit()
        return new_state

    def get_joke_by_id(self, joke_id):
        select_string = "SELECT * FROM jokes WHERE _id = ?"
        self.cursor.execute(select_string, (joke_id,))
        joke_tuple = self.cursor.fetchone()
        return Joke.map_from_tuple(joke_tuple)

    def set_comedian_of_joke(self, joke_id, comedian):
        comedian_tuple = self.get_comedian(comedian)
        if comedian_tuple is None:
            raise ValueError("Comedian by name '" + comedian + "' does not exist in database")
        j = self.get_joke_by_id(joke_id)
        previous_comedian = j.comedian
        if previous_comedian == comedian:
            return
        previous_comedian_tuple = self.get_comedian(previous_comedian)
        
        update_string = "UPDATE jokes SET comedian = ? WHERE _id = ?"
        self.cursor.execute(update_string, (comedian, joke_id))
        self.set_n_jokes(comedian, comedian_tuple[1] + 1)
        self.set_n_jokes(previous_comedian, previous_comedian_tuple[1] - 1)
        self.connection.commit()

    def set_n_jokes(self, comedian, n):
        update_string = "UPDATE comedians SET n_jokes = ? WHERE name=?"
        self.cursor.execute(update_string, (n, comedian))        
        self.connection.commit()
        
    def save_joke(self, joke):
        insert_string = '''INSERT INTO jokes (comedian, punchline, context, date, participants, payed_for)
        VALUES (?,?,?,?,?,?)
        '''
        insert_tuple = (joke.comedian,
                        joke.punchline,
                        joke.context,
                        joke.date,
                        joke.participants,
                        joke.payed_for)
        self.cursor.execute(insert_string, insert_tuple)
        update_string = "UPDATE comedians SET n_jokes = n_jokes + 1 WHERE name=?"
        self.cursor.execute(update_string, (joke.comedian,))        
        self.connection.commit()

    def delete_joke(self, _id):
        delete_string = "DELETE FROM jokes WHERE _id = ?;"
        self.cursor.execute(delete_string, (_id,))
        self.connection.commit()

    def add_comedian(self, name):
        insert_string = "INSERT INTO comedians VALUES (?, 0)"
        insert_tuple = (name,)
        self.cursor.execute(insert_string, insert_tuple)
        self.connection.commit()

    def get_comedian(self, name):
        fetch_string = "SELECT * FROM comedians WHERE name=?"
        self.cursor.execute(fetch_string, (name,))
        comedian = self.cursor.fetchone()        
        return comedian

    def get_comedians(self):
        fetch_string = "SELECT * FROM comedians ORDER BY name ASC"
        self.cursor.execute(fetch_string)
        comedians_fetched = self.cursor.fetchmany()        
        return comedians_fetched

    def close(self):
        self.connection.close()
