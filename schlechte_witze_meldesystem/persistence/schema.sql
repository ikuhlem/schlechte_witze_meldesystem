-- noinspection SqlNoDataSourceInspectionForFile

DROP TABLE IF EXISTS comedians;
DROP TABLE IF EXISTS jokes;
DROP TABLE IF EXISTS withdrawals;
DROP TABLE IF EXISTS users;


CREATE TABLE comedians (
       name text PRIMARY KEY NOT NULL,
       n_jokes int
);
    
CREATE TABLE jokes (
    _id integer PRIMARY KEY AUTOINCREMENT,
    comedian text NOT NULL,
    punchline text NOT NULL,
    context text,
    date text NOT NULL,
    participants text,
    payed_for integer,
    FOREIGN KEY (comedian) REFERENCES comedians(name)
);

    
CREATE TABLE withdrawals (
    _id integer PRIMARY KEY NOT NULL,
    date text NOT NULL,
    amount integer NOT NULL,
    reason text NOT NULL
);
    
CREATE TABLE users (
    _id integer PRIMARY KEY AUTOINCREMENT,
    name text NOT NULL,
    password_hash text NOT NULL,
    activated integer NOT NULL
);
