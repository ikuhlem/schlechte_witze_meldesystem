"""
USAGE: 
   o install in develop mode: navigate to the folder containing this file,
                              and type 'python setup.py develop --user'.
                              (ommit '--user' if you want to install for 
                               all users)                           
"""
from setuptools import setup

setup(name='schlechte_witze_meldesystem',
      version='0.1.6',
      description='Report bad jokes and store them in a database.',
      url='',
      author='Ilyas Kuhlemann',
      author_email='ilyasp.ku@gmail.com',
      license='MIT',
      include_package_data=True,
      packages=["schlechte_witze_meldesystem",
                "schlechte_witze_meldesystem.persistence",
                "schlechte_witze_meldesystem.web",
                "schlechte_witze_meldesystem.model",
                "schlechte_witze_meldesystem.app"],
      entry_points={
          "console_scripts": [
          ],
          "gui_scripts": [
          ]
      },
      install_requires=["flask",
                        "flask-wtf",
                        "nose"],
      zip_safe=False)
