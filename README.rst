

Deploy on uberspace
===================

Commands I used on uberspace:

.. code::

   virtualenv venvs/schlecht
   source ~/venvs/schlecht/bin/activate
   pip install flask waitress
   cd web_apps/
   pip install schlechte_witze_meldesystem-0.1.0-py3-none-any.whl 
   # probably put the result of this in some app config,
   # but I forgot where exactly:
   python -c 'import os; print(os.urandom(16))'
   export FLASK_APP=schlechte_witze_meldesystem
   flask init-db
   uberspace web backend set /schlecht --http --port 8081 --remove-prefix
   waitress-serve --listen=0.0.0.0:8081 --call 'schlechte_witze_meldesystem:create_app'
   cd services.d/
   tail -n 3 uwsgi.ini >> flask.ini
   cat flask.ini 
   emacs flask.ini 
   supervisorctl reread
   supervisorctl update
   supervisorctl status
   pip install uwsgi
   uberspace web backend set /schlecht --http --port 1024
   deactivate
   pip3 install --user schlechte_witze_meldesystem-0.1.1-py3-none-any.whl 
   mv ~/uwsgi/apps-enabled/schlecht.ini ./
   supervisorctl reread
   supervisorctl update
   supervisorctl status
   source ~/venvs/schlecht/bin/activate
   uwsgi schlecht.ini
   emacs ~/venvs/schlecht/var/schlechte_witze_meldesystem-instance/config.py
   pip install schlechte_witze_meldesystem-0.1.4-py3-none-any.whl 
   supervisorctl restart flask
   supervisorctl tail flask stderr
   supervisorctl status
   emacs ~/venvs/schlecht/var/schlechte_witze_meldesystem-instance/config.py
   uwsgi schlecht.ini
   export SCRIPT_NAME="/schlecht"
   export FLASK_APP=schlechte_witze_meldesystem
   uwsgi --manage-script-name schlecht.ini --mount /schlecht=schlechte_witze_meldesystem:app
   mv schlechte_witze.db venvs/schlecht/var/schlechte_witze_meldesystem-instance/witze.sqlite 



